# -*- coding: utf-8 -*-

"""
Cree le mercredi 01/04/2020

@author: L'Equipe Rocco // Corentin BRIAND
"""

import os
import sys
sys.path.append('../Abstract/')
sys.path.append('../Presentation/')

import AbstractPiste
import PresentationPiste
import tkinter.filedialog

class Controller():
    """
    """
    def __init__(self,topPresentation):
        """
        Constructeur du controller des pistes
        
        
        Parametre
        ----------
        topPresentation : Master
            Zone d'affichage des presentations
        """
        self.topPresentation=topPresentation
        self.Thread = None
        self.Prese = None
        self.Piste = None
        self.Count = 0
        
    def removeChannelID(self,ID):
        """
        Supprimer la piste et la presentation ayant l'id en parametre
        
        
        Parametre
        ----------
        ID : int
            L'Id de la piste a supprimer
        """
        deletedPrese = self.getPresentation(ID)
        deletedPiste = self.getPiste(ID)
        if(deletedPiste != None):
            if(deletedPiste.getPrecedent() == None):
                self.Piste=deletedPiste.getSuivant()
                self.Prese=deletedPrese.getSuivant()
            else:
                deletedPiste.getPrecedent().setSuivant(deletedPiste.getSuivant())
                deletedPrese.getPrecedent().setSuivant(deletedPrese.getSuivant())
            if(deletedPiste.getSuivant() != None):
                deletedPiste.getSuivant().setPrecedent(deletedPiste.getPrecedent())
                deletedPrese.getSuivant().setPrecedent(deletedPrese.getPrecedent())
            deletedPiste.destroy()
            deletedPrese.remove()
            self.Count -= 1
            self.updateID()
        
    def updateID(self):
        """
        Maj des ID des pistes et de la presentation
        """
        id = 1
        changedPrese=self.Prese
        changedPiste=self.Piste
        while(changedPiste != None):
            changedPrese.setID(id)
            changedPiste.setID(id)
            changedPiste = changedPiste.getSuivant()
            changedPrese = changedPrese.getSuivant()
            id += 1

    def getPiste(self,ID):
        """
        Retourne la piste ayant l'id corespondant au parametre
        
        
        Parametre
        ----------
        ID : int
            L'Id de la piste a trouver
        
        
        Return
        ----------
        AbstractPiste : La piste ayant l'id demande ou None
        """
        getPiste = self.Piste
        while(getPiste != None and getPiste.getID() != ID):
            getPiste=getPiste.getSuivant()
        if(getPiste == None):
            return None
        else:
            return getPiste
        
    def getPresentation(self,ID):
        """
        Retourne la presentation ayant l'id corespondant au parametre
        
        
        Parametres
        ----------
        ID : int
            L'Id de la presentation a trouver
        
        
        Return
        ----------
        Presentation : La presentation ayant l'id demande ou None
        """
        getPrese = self.Piste
        while(getPrese != None and getPrese.getID() != ID):
            getPrese=getPrese.getSuivant()
        if(getPrese == None):
            return None
        else:
            return getPrese
        
    def addChannels(self,nbSupplementaire):
        """
        Ajoute de channel(s) a la liste chainer
        
        Parametre
        ----------
        nbSupplementaire : int
            Le nombre de channel(s) à ajouter
            
        """
        lastPrese = self.getPresentation(self.count)
        lastPiste = self.getPiste(self.count)
        if(lastPiste != None):
            count = 0
            while(count < nbSupplementaire):
                newPrese = Presentation(self, self.topPresentation , self.count)
                newPiste = AbstractPiste(self.count)
                lastPrese.setSuivant(newPrese)
                lastPiste.setSuivant(newPiste)
                newPrese.setPrecedent(lastPrese)
                newPiste.setPrecedent(lastPiste)
                lastPrese = newPrese
                lastPiste = newPiste
                count += 1
                self.count += 1
                
    def removeChannels(self,nbSupprimer):
        """
        Supprime des channel(s) de la liste chainer
        
        Parametre
        ----------
        nbSupprimer : int
            Le nombre de channel(s) à supprimer
            
        """
        lastPrese = self.getPresentation(self.count)
        lastPiste = self.getPiste(self.count)
        count = 0
        while(lastPiste != None and count < nbSupprimer):
            deletePrese = lastPrese
            deletePiste = lastPiste
            lastPrese = deletePrese.getPrecendent()
            lastPiste = deletePiste.getPrecendent()
            lastPrese.setSuivant(None)
            lastPiste.setSuivant(None)
            deletePrese.remove()
            deletePiste.destroy()
            count += 1
            self.count -= 1
            
            
#=====================================================================================
    def listenerVolume(self,Volume,ID):
        changedPiste = self.getPiste(ID)
        if(changedPiste != None):
            changedPiste.setVolume(Volume)
            
    def listenerStereo(self,Stereo,ID):
        changedPiste = self.getPiste(ID)
        if(changedPiste != None):
            changedPiste.setStereo(Stereo)
            
    def listenerPitch(self,Pitch,ID):
        changedPiste = self.getPiste(ID)
        if(changedPiste != None):
            changedPiste.setPitch(Pitch)
            
    def listenerMute(self,Mute,ID):
        changedPiste = self.getPiste(ID)
        if(changedPiste != None):
            changedPiste.setMute(Mute)
            
    def listenerLoad(self,ID):  #Methode permettant d'ouvrir un fichier de type wav ou mp3 et de le charger en tant que "track"
        changedPiste = self.getPiste(ID)
        changedPrese = self.getPresentation(ID)
        if(changedPiste != None):
            newTrackName = tkinter.filedialog.askopenfile(filetypes=[('wav file','.wav'),('mp3 file','.mp3')],title='Choose a file', initialdir=os.getcwd()+"/lib/")
            changedPrese.setName(os.path.basename(self.soundFile.name))
            
            #Creation du Track
            newTrack=newTrackName #Temporaire
            
            changedPiste.setTrack(newTrack)