import sys
import os
from tkinter import *


sys.path.append('Pistes/Controller/')
sys.path.append('Pistes/Abstract/')
sys.path.append('Pistes/Presentation/')

sys.path.append('Top/Controller/')
sys.path.append('Top/Abstract/')
sys.path.append('Top/Presentation/')

from ControllerTop import ControllerTop

#from Sources.Top.Controller.ControllerTop import ControllerTop


class test ( Frame ):
    def __init__( self , master):
        super().__init__(master)
        self.master = master
        self.controller = ControllerTop(self.master)



# Construction de la fenêtre principale «root»
root = Tk()
app = test(root)
root.iconbitmap(os.path.join(os.path.abspath(os.path.dirname("..\Images\DJAhled.ico")), "DJAhled.ico"))
root.configure(background='#FFF')

# Lancement de la «boucle principale»
root.mainloop()


