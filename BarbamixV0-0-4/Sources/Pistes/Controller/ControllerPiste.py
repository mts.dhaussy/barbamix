# -*- coding: utf-8 -*-

"""
Cree le mercredi 01/04/2020

@author: L'Equipe Rocco // Corentin BRIAND
"""

import os



from AbstractPiste import AbstractPiste
from PresentationPiste import PresentationPiste
from ThreadChannel import ThreadChannel

##from ..Abstract.AbstractPiste import AbstractPiste
##from ..Presentation.PresentationPiste import PresentationPiste 
##from .ThreadChannel import ThreadChannel

from tkinter import filedialog

class ControllerPiste():
    """
    """
    def __init__(self,topPresentation):
        """
        Constructeur du controller des pistes
        
        
        Parametre
        ----------
        topPresentation : Master
            Zone d'affichage des presentations
        """
        self.topPresentation=topPresentation
        self.Thread = None
        self.Prese = None
        self.Piste = None
        self.Count = 1
        self.VolumeGlobal = 100
        
    def removeChannelID(self,ID):
        """
        Supprimer la piste, la presentation et le thread ayant l'id en parametre
        
        
        Parametre
        ----------
        ID : int
            L'Id de la piste a supprimer
        """
        deletedPrese = self.getPresentation(ID)
        deletedPiste = self.getPiste(ID)
        deletedThread = self.getThread(ID)
        if(deletedPiste != None):
            if(ID == 1):
                self.Piste=deletedPiste.getSuivant()
                self.Prese=deletedPrese.getSuivant()
                self.Thread=deletedThread.getSuivant()
            else:
                deletedPiste.getPrecedent().setSuivant(deletedPiste.getSuivant())
                deletedPrese.getPrecedent().setSuivant(deletedPrese.getSuivant())
                deletedThread.getPrecedent().setSuivant(deletedThread.getSuivant())
            if(deletedPiste.getSuivant() != None):
                deletedPiste.getSuivant().setPrecedent(deletedPiste.getPrecedent())
                deletedPrese.getSuivant().setPrecedent(deletedPrese.getPrecedent())
                deletedThread.getSuivant().setPrecedent(deletedThread.getPrecedent())
            deletedPiste.remove()
            deletedPrese.remove()
            deletedThread.remove()
            self.Count -= 1
            self.updateID()
        
    def updateID(self):
        """
        Maj des ID des pistes, threads et presentations
        """
        id = 1
        changedPrese=self.Prese
        changedPiste=self.Piste
        changedThread=self.Thread
        while(changedPiste != None):
            changedPrese.setID(id)
            changedPiste.setID(id)
            changedThread.setID(id)
            changedPiste = changedPiste.getSuivant()
            changedPrese = changedPrese.getSuivant()
            changedThread = changedThread.getSuivant()
            id += 1

    def getPiste(self,ID):
        """
        Retourne la piste ayant l'id corespondant au parametre
        
        
        Parametre
        ----------
        ID : int
            L'Id de la piste a trouver
        
        
        Return
        ----------
        AbstractPiste : La piste ayant l'id demande ou None
        """
        getPiste = self.Piste
        while(getPiste != None and getPiste.getID() != ID):
            getPiste=getPiste.getSuivant()
            
        return getPiste
        
    def getPresentation(self,ID):
        """
        Retourne la presentation ayant l'id corespondant au parametre
        
        
        Parametres
        ----------
        ID : int
            L'Id de la presentation a trouver
        
        
        Return
        ----------
        Presentation : La presentation ayant l'id demande ou None
        """
        getPrese = self.Prese
        while(getPrese != None and getPrese.getID() != ID):
            getPrese=getPrese.getSuivant()
        return getPrese
        
        
    def getThread(self,ID):
        """
        Retourne le thread ayant l'id corespondant au parametre
        
        
        Parametres
        ----------
        ID : int
            L'Id du thread a trouver
        
        
        Return
        ----------
        Thread : La thread ayant l'id demande ou None
        """
        getThread = self.Thread
        while(getThread != None and getThread.getID() != ID):
            getThread=getThread.getSuivant()
        return getThread
        
        
        
    def addPistes(self,nbSupplementaire):
        """
        Ajout de piste(s) a la liste chainer
        
        Parametre
        ----------
        nbSupplementaire : int
            Le nombre de channel(s) à ajouter
            
        """
        lastPrese = self.getPresentation(self.Count-1)
        lastPiste = self.getPiste(self.Count-1)
        lastThread = self.getThread(self.Count-1)
        count = 0
        if(self.Count == 1 and nbSupplementaire > 0):
            self.Prese = PresentationPiste(self, self.topPresentation , self.Count)
            self.Piste = AbstractPiste(self.Count)
            self.Thread = ThreadChannel(self.Count)
            lastPrese = self.Prese
            lastPiste = self.Piste
            lastThread = self.Thread
            count = 1
            self.Count = 2
            
        while(count < nbSupplementaire):
            newPrese = PresentationPiste(self, self.topPresentation , self.Count)
            newPiste = AbstractPiste(self.Count)
            newThread = ThreadChannel(self.Count)
            lastPrese.setSuivant(newPrese)
            lastPiste.setSuivant(newPiste)
            lastThread.setSuivant(newThread)
            newPrese.setPrecedent(lastPrese)
            newPiste.setPrecedent(lastPiste)
            newThread.setPrecedent(lastThread)
            lastPrese = newPrese
            lastPiste = newPiste
            lastThread = newThread
            count += 1
            self.Count += 1
                
    def removePistes(self,nbSupprimer):
        """
        Supprime des piste(s) de la liste chainer
        
        Parametre
        ----------
        nbSupprimer : int
            Le nombre de channel(s) à supprimer
            
        """
        lastPrese = self.getPresentation(self.Count-1)
        lastPiste = self.getPiste(self.Count-1)
        lastThread = self.getThread(self.Count-1)
        count = 0
        while(lastPiste != None and count < nbSupprimer):
            deletePrese = lastPrese
            deletePiste = lastPiste
            deleteThread = lastThread
            lastPrese = deletePrese.getPrecendent()
            lastPiste = deletePiste.getPrecendent()
            lastThread = deleteThread.getPrecendent()
            lastPrese.setSuivant(None)
            lastPiste.setSuivant(None)
            lastThread.setSuivant(None)
            deletePiste.remove()
            deletePrese.remove()
            deleteThread.remove()
            count += 1
            self.Count -= 1
            
#=====================================================================================

    def listenerVolume(self,Volume,ID):
        changedPiste = self.getPiste(ID)
        changedThread = self.getThread(ID)
        if(changedPiste != None):
            changedPiste.setVolume(Volume*self.VolumeGlobal/100)
            if(not(changedPiste.getMute())):
                changedThread.setAttribute("volume",Volume*self.VolumeGlobal/100)
                
    def listenerVolumeGlobal(self,Volume):
        self.VolumeGlobal=Volume
        changedPiste = self.Piste
        changedThread = self.Thread
        while(changedPiste != None):
            changedPiste.setVolume(Volume*self.VolumeGlobal/100)
            if(not(changedPiste.getMute())):
                changedThread.setAttribute("volume",Volume*self.VolumeGlobal/100)
            changedPiste=changedPiste.getSuivant()
            changedThread=changedThread.getSuivant()
            
    def listenerStereo(self,Stereo,ID):
        changedPiste = self.getPiste(ID)
        changedThread = self.getThread(ID)
        if(changedPiste != None):
            changedPiste.setStereo(Stereo)
            changedThread.setAttribute("stereo",-Stereo)
            
    def listenerPitch(self,Pitch,ID):
        changedPiste = self.getPiste(ID)
        changedThread = self.getThread(ID)
        if(changedPiste != None):
            changedPiste.setPitch(Pitch)
            changedThread.setAttribute("pitch",Pitch)
            
    def listenerMute(self,Mute,ID):
        changedPiste = self.getPiste(ID)
        changedThread = self.getThread(ID)
        if(changedPiste != None):
            changedPiste.setMute(Mute)
            if(Mute):
                changedThread.setAttribute("volume",0)
            else:
                changedThread.setAttribute("volume",changedPiste.getVolume()*self.VolumeGlobal/100)
            self.getPresentation( ID ).refreshMuteState( Mute )
            
    def listenerLoadTrack(self,ID):  #Methode permettant d'ouvrir un fichier de type wav ou mp3 et de le charger en tant que "track"
        changedPiste = self.getPiste(ID)
        changedPrese = self.getPresentation(ID)
        changedThread = self.getThread(ID)
        if(changedPiste != None):
            newTrackName = filedialog.askopenfile(filetypes=[('wav file','.wav'),('mp3 file','.mp3')],title='Choose a file', initialdir=os.getcwd()+"/lib/")
            changedPrese.setName(os.path.basename(newTrackName.name))
            changedThread.setSound(newTrackName.name)

#=====================================================================================

    def getVolume(self,ID):
        return self.getPiste(ID).getVolume()
    
    def setVolume(self,ID,volume):
        self.listenerVolume(volume,ID)
        
    def getStereo(self,ID):
        return self.getPiste(ID).getStereo()
    
    def setStereo(self,ID,stereo):
        self.listenerStereo(stereo,ID)
        
    def getPitch(self,ID):
        return self.getPiste(ID).getPitch()
    
    def setPitch(self,ID,pitch):
        self.listenerPitch(pitch,ID)
        
    def getMute(self,ID):
        return self.getPiste(ID).getMute()
    
    def setMute(self,ID,mute):
        self.listenerMute(mute,ID)
        
    def getTrackPath(self,ID):
        return self.getThread(ID).getTrack().getSource()
    
    def setTrackPath(self,ID,path):
        self.getThread(ID).setSound(path)
        
    def getEffet(self,ID):
        return self.getPiste(ID).getEffet()
    
    def setEffet(self,ID,effet):
        self.getPiste(ID).setEffet(effet)
        
    def getCrossFade(self,ID):
        return self.getPiste(ID).getCrossFade()
    
    def setCrossFade(self,ID,crossFade):
        self.getPiste(ID).setCrossFade(crossFade)
        
    def getRandomState(self,ID):
        return self.getPiste(ID).getRandomState()
    
    def setRandomState(self,ID,randomState):
        self.getPiste(ID).setRandomState(randomState)
        
    def getRandomPlay(self,ID):
        return self.getPiste(ID).getRandomPlay()
    
    def setRandomPlay(self,ID,randomPlay):
        self.getPiste(ID).setRandomPlay(randomPlay)





