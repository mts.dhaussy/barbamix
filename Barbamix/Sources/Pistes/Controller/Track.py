# -*- coding: utf-8 -*-

"""
@author: Nathan Mingoube & Ferragu Thomas
"""

# importe le module graphique TK


from tkinter import *
import os
import tkinter as tk
import tkinter.filedialog
from pyo import *
from time import *
from os import path


# @Author: Nathan Mingoube & Ferragu Thomas, Groupe trébuchet
# @Name: Track class
# @Date: 02/02/2020
# @Version: 1.0.0
# @Dependencies: pyo librairies


class Track:

    def __init__(self, pth):  # Constructor, overrides setSource()

        self.__pitch = 1.0  # Pitch (highness) of the sound
        self.__xfade = 25  # Crossfade (in % of total duration)
        self.__mul = 1  # Multiplicator of sound speed OVERRIDEN
        self.__start = 0  # Offset to start
        self.__level = 20  # Sound level modifier
        self.__gain = 0  # Gain modifier
        self.__pan = 0.0  # PAN of the sound [-1.0;1.0]. Set to -1.0 to have the sound 100% to the right or 1.0 to have the sound 100% to the left (may be the other way around)
        self.__path = pth  # Absolute path to sound file
        self.__srcTable = SndTable(self.__path)  # Sound file
        self.__looper = Looper(self.__srcTable)  # Instance of the sound
        self.__timeStart = 0;
        self.__timePause = 0;

    def setPitch(self, pitch):  # Sets pitch of the sound
        self.__pitch = pitch

    def getPitch(self):  # Return pitch of the sound
        return self.__pitch

    def setXFade(self, xFade):  # Sets xFade of the sound
        self.__xfade = xFade

    def getXFade(self):  # Return xFade of the sound
        return self.__xfade

    def setMul(self, mul):  # Sets Mul of the sound
        self.__mul = mul

    def getMul(self):  # Return Mul of the sound
        return self.__mul

    def setStart(self, start):  # Sets start point of the sound
        self.__start = start

    def getStart(self):  # Return start point of the sound
        return self.__start

    def setLevel(self, lv):  # Sets relative level of the sound in dB
        self.__srcTable.mul = lv
        self.__level = self.__level + lv

    def getLevel(self):  # Returns level of the sound in dB
        return self.__level

    def setGain(self, gain):  # Sets gain of the sound
        self.__gain = gain

    def getGain(self):  # Return gain of the sound
        return self.__gain

    def setPan(self, pn):  # Sets the pan (stereo)
        self.__pan = pn

    def getPan(self):  # Returns the pan (stereo)
        return self.__pan

    def setSource(self, path):  # Sets source to the audio file
        self.__path = path
        self.__srcTable = SndTable(path)

    def getSource(self):  # Returns source file
        return self.__path

    def getSoundDuration(self):  # Returns the duration of the sound file
        return self.__srcTable.getDur()

    def __initSound(self, time=0):  # Initiates the looper class and summons its controls
        if (time == 0):
            time = self.__start
        self.__srcTable = SndTable(path=self.__path, start=time)
        self.__looper = Looper(table=self.__srcTable, pitch=self.__pitch, dur=180, xfade=self.__xfade, autosmooth=True,
                               mul=[self.__level * (1 + self.__pan), self.__level * (1 - self.__pan)])

    def playSound(self):  # Plays the sound
        if (self.__timePause == 0):
            self.__initSound()
            self.__timeStart = time()
        else:
            self.__initSound(time=self.__timePause - self.__timeStart)
            self.__timeStart = time()
            self.__timePause = 0

        self.__looper = self.__looper.mix(2).out()
        self.__timeStart = time()

    def pause(self):
        self.__looper = self.__looper.stop()
        self.__timePause = time()

    def export(self):
        res={
                "level" : self.__level,
                "gain" : self.__gain,
                "pan" : self.__pan,
                "pitch" : self.__pitch,
                "xfade" : self.__xfade,
                "start" : self.__start,
                "path" : self.__path
            }
        return res


s = Server().boot()                          #Starts audio engine
s.start()
"""
track1 = Track("C:/Users/Nicolas/PycharmProjects/untitled/test.wav")

track1.playSound()
sleep(1) # Read sound for 1 sec
track1.pause()
sleep(2) # Pause for 2 sec
track1.playSound()
"""
