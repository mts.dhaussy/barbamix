from threading import Thread

from Track import Track
#from .Track import Track

from pyo import *

class ThreadChannel(Thread):
    
    """
    Thread pour un canal 
    (1 canal = 1 ThreadChannel)
    """
    
    def __init__(self, id):
        Thread.__init__(self)
        self.precedentPiste = None
        self.suivantPiste = None
        self.id = id
        self.track = Track(os.path.join(os.path.abspath(os.path.dirname("..\Banque\\")), "626.wav"))
        #Creation du thread pour la lecture du son
        self.thPlaySound = ThreadPlaySound(self.track)
        #Creation du thread pour la modification du son
        self.thModifySound = ThreadModifySound(self.thPlaySound, self.track)
        
    def run(self):
        #Lancement des threads
        self.thPlaySound.start()
        self.thModifySound.start()
        
    def getThPlaySound(self):
        """
        Retourne le thPlaySound, le thread qui s'occupe de jouer/mettre en pause le son
        
        
        Return
        ----------
        ThreadModifySound : Le thread de modification du son
        """
        return self.thPlaySound
    
    def getThModifySound(self):
        """
        Retourne le thModifySound, le thread qui s'occupe de la modification du son
        
        
        Return
        ----------
        ThreadModifySound : Le thread de modification du son
        """
        return self.thModifySound
    
    def getID(self):
        """
        Retourne l'ID de la piste
        
        
        Return
        ----------
        int : L'ID de la piste
        """
        return self.id
    
    def setID(self,id):
        """
        Change l'ID de la piste
        
        
        Parametres
        ----------
        id : int
            Le nouvel ID de la piste
        """
        self.id = id

    def getSuivant(self):
        """
        Retourne la piste suivante
        
        
        Return
        ----------
        AbstractPiste : La piste suivante
        """
        return self.suivantPiste
    
    def setSuivant(self,suivant):
        """
        Change la piste suivant
        
        
        Parametres
        ----------
        suivant : AbstractPiste
            La nouvelle piste suivante
        """
        self.suivantPiste=suivant
            
    def getPrecedent(self):
        """
        Retourne la piste précédente
        
        
        Return
        ----------
        AbstractPiste : La piste précédente
        """
        return self.precedentPiste
    
    def setPrecedent(self,precedent):
        """
        Change le channel précédent de la piste
        
        
        Parametres
        ----------
        precedent : AbstractPiste
            La nouvelle piste précédent
        """
        self.precedentPiste=precedent
    
    def getTrack(self):
        return self.track

    def setAttribute(self, attr, val):
        self.thModifySound.setAttribute(attr, val)
    
    def play(self):
        self.thPlaySound.play()
    
    def pause(self):
        self.thPlaySound.pause()

    def setSound(self, pth):
        self.thPlaySound.setSound(pth)

    def remove(self):
        del(self)

class ThreadPlaySound(Thread):
    
    """
    Thread pour la lecture du son 
    """
    
    def __init__(self, trck):
        Thread.__init__(self)
        self.__track__ = trck
    
    def run(self):
        print("run playSound")
        self.__track__.playSound()                     #Lancement du son
    
    def play(self):
       self.__track__.playSound()                      #Lancement du son

    def pause(self):
        self.__track__.pause()
    
    def setSound(self, pth):
        self.__track__.setSource(pth)
   
        
class ThreadModifySound(Thread):
    
    """
    Thread pour la modification du son
    """
    
    def __init__(self, thPlaySound, trck):
        Thread.__init__(self)
        self.__track__=trck
        self.__sound__=thPlaySound

    def run(self):
        print("run modifySound")


    def setAttribute(self, attr, val):
        """
        Modifie un attribut de la track
        
        
        Parametres
        ----------
        attr : l'attribut à modifier
        val : la valeur à donner à l'attribut
        """
        self.__sound__.pause()
        if attr == 'volume':
            self.__track__.setLevel(val)
        elif attr == 'crossfade':
            self.__track__.setXFade(val)
        elif attr == 'stereo':
            self.__track__.setPan(val)
        elif attr == 'pitch':
            self.__track__.setPitch(val)

        self.__sound__.play()
