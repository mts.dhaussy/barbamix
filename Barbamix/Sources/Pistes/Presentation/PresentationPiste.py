from tkinter import *

from knob import Knob
#from .knob import Knob
import sys
import os.path
sys.path.append('../')


"""
Documentation de la classe Presentation pour une piste

	Version : 15/04/2020 16h Thibaud BARON Equipe ROCCO

	Participant : Thibaud BARON

	Description :

		Présentation est une classe concrète qui va permettre d'afficher les boutons, les informations ,... à l'utilisateur pour une seule piste associée.
		Présentation connait la présentation de la piste suivante et la présentation de la piste précédente, ce qui permet former une liste chainée.

	Attribut :

		topPresentation : topPresentation où sera affichée la présentation de la piste
		idPiste : Numéro de la piste
		controller : Controller de la piste

		suivant : Presentation de la piste suivant  
		precedent : Presentation de la piste precedent
		
		idLabel : Texte affichant le numéro de la piste
		nameLabel : Texte affichant le nom du son
		loadButton : Bouton pour charger un son
		stereoLabel : Texte affichant "Stéréo"
		stereoKnob : bouton tournant pour sélectionner la valeur de la stéréo
		volumeLabel : Texte affichant "Volume"
		soundSlider : curseur pour sélectionner la valeur du volume
		speedLabel : Texte affichant "Pitch"
		speedKnob : Knob pour sélectionner la valeur de vitesse
		muteLabel : Texte pour afficher le "Mute"
		muteButtonState : Boolean pour si le son est mise en sourdine
		muteButton : Bouton pour mettre en sourdine
		effectButton : Bouton pour les effets
		crossFadeButton : Bouton pour activer le crossFade
		randomButton : Button pour activer le random

	Constructeur:
		PresentationPiste(controller, topPresentation , idPiste)

	Methode :
		open_file(self) : Méthode qui signale l'importation d'un son.
		setName(self, name) : Change le nom à afficher
		remove(self) : Méthode qui détruit la présentation
		setId(self,id) : Méthode qui change le numéro d'identité
		getId(self) : Méthode qui renvoie le numéro d'identité
		getSuivant(self) : Méthode qui renvoie la présentation de la piste suivante
		setSuivant(self,suivant) : Méthode qui change la présentation de la piste suivante
		getPrecedent(self) : Méthode qui renvoie la présentation de la piste précédente
		setPrecedent(self) : Méthode qui renvoie la présentation de la piste précédente
		isMuted(self) : Méthode qui renvoie le boolean de si le son est mute ou pas, c'est à dire si le bouton mute est enfoncé.
		refreshMuteState(self,muteState) : Méthode qui met à jour l'état du bouton mute
		setVolumeValue(self,volumeValue) : Méthode qui change la valeur du VolumeSlider.
		setStereoValue(self,stereoValue) : Méthode qui change la valeur du StéréoKnob.
		setSpeedValue( self,SpeedValue ) : Méthode qui change la valeur du SpeedValue.


			--- Méthodes associées aux boutons ---

		modifyStereoValue(self) : Méthode qui signale de modifier la valeur du Stéréo
		modifySpeedValue(self) : Méthode qui signale modifier la valeur de la vitesse
		modifyVolumeValue(self) : Méthode qui signale modifier la valeur du volume
		modifyEffect(self) : Méthode qui signale une demande de modifier l'effet. #TODO : A faire 
		modifyCrossFade(self) : Méthode qui signale une demande de CrossFade. #TODO : A faire
		modifyRandom(self) : Méthode qui signale une demande de Random. #TODO : A faire
		askRemovePiste(self) : Méthode qui signale une demande de retirer la piste.

"""


class PresentationPiste (Frame):

	"""
		Constructeur
		
		Paramètres
		------------------
			Presentation		self : Objet créé
			Controller			controller : Controller de la piste
			top.Presentation	topPresentation : Presentation du top où sera affichée Presentation de la piste.
			int					idPiste : Numéro attribué à la piste pour se repérer
	"""	
	
	def __init__( self ,controller, topPresentation , idPiste):
		super().__init__(topPresentation)
		
		self.topPresentation = topPresentation
		self.controller = controller

		self.idPiste = idPiste
		self.suivant = None
		self.precedent = None

		self.idLabel = Label( self , text = "CH " + str( self.idPiste ) )
		self.idLabel.pack()

		self.nameLabel = Label( self , text="Veuillez charger un son")
		self.nameLabel.pack()

		self.LoadButton = Button( self , text="Charger" , command = self.open_file )
		self.LoadButton.pack()

		self.removeButton = Button( self , text="Supprimer" , command = self.askRemovePiste )
		self.removeButton.pack()

		self.stereoLabel = Label(self,text="Stéréo")
		self.stereoLabel.pack()

		self.stereoKnob = Knob(self, -1 , 1 , self.modifyStereoValue)
		self.stereoKnob.pack()

		self.volumeSlider = Scale(self , orient=VERTICAL, command = self.modifyVolumeValue )
		self.volumeSlider.config(from_ = 100, to = 0)
		self.setVolumeValue(100)
		self.volumeSlider.pack()

		self.speedLabel = Label(self, text="Pitch")
		self.speedLabel.pack()

		self.speedKnob = Knob( self , 0 , 2 , self.modifySpeedValue)
		self.speedKnob.pack()

		

		self.muteLabel = Label( self , text = "Mute")
		self.muteLabel.pack()

		self.muteButtonState = False
		soundOn=PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\\Images\\unMute.png")), "unMute.png"),master=self.topPresentation)
		self.muteButton = Button(self,image=soundOn, command = self.modifyMuteState)
		self.muteButton.image = soundOn
		self.muteButton.pack()

		self.effectButton = Button(self,text="Effet")
		self.effectButton.pack()

		self.crossFadeButton = Button(self,text="CrossFade")
		self.crossFadeButton.pack()
		self.randomButton = Button(self,text="Random")
		self.randomButton.pack()

		self.configure(background="#FFF")
		self.pack(side = LEFT)

		
	def open_file(self):
		"""
		Méthode qui signale l'importation d'un son.
		"""
		self.controller.listenerLoadTrack ( self.idPiste )

	
	def setName(self, name):
		"""
		Méthode qui change le nom affiché du son

		Paramètre
		-----------
		name : String nom à afficher

		"""
		self.nameLabel.config(text = name)

	
	def remove(self):
		"""
		Méthode qui détruit la presentation.
		"""
		self.destroy()
	

	def setID(self,id):
		"""
		Méthode qui attribut le numéro d'identifiant de la piste

		Paramètre
		----------
		id : int numéro d'identification de la piste
		"""
		self.idPiste = id
		self.idLabel.configure(text ="CH "+ str(self.idPiste))
	

	def getID(self):
		"""
		Méthode qui renvoie le numéro d'identifiant de la piste

		Return
		------------
		int numéro d'identification de la piste
		"""
		return self.idPiste
	

	def getSuivant(self):
		"""

		Méthode qui renvoie la Presentation de la piste suivante

		Return
		--------------
		Presentation Presentation de la piste suivante

		"""
		return self.suivant

	
	def setSuivant(self,suivant):
		"""
		Méthode qui attribut la Presentation de la piste suivante

		Parametres
		------------------
		suivant : Presentation de la piste suivante
		"""
		self.suivant=suivant

	
	def getPrecedent(self):
		"""
		Méthode qui renvoie la Presentation de la piste précédente

		Return
		--------------
		Presentation Presentation de la piste précédente

		"""
		return self.precedent

	
	def setPrecedent(self,precedent):
		"""
		Méthode qui attribut la Presentation de la piste précédente
		
		Parametres
		------------
		precedent : Presentation de la piste précédente

		"""
		self.precedent=precedent

	
	def isMuted(self):
		"""
		Méthode qui retourne le boolean de si ça a été mise en sourdine

		Return
		------------
		boolean : si le son est en sourdine
		"""
		return self.muteButtonState
	
	
	def refreshMuteState(self,muteState):
		"""
		Méthode qui met à jour l'état du bouton mute

		Parametres
		------------
		muteState : boolean nouvelle état du bouton mute
		"""
		if muteState:
			soundOff=PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\\Images\\mute.png")), "mute.png"),master=self.topPresentation)
			self.muteButton.config(image=soundOff)
			self.muteButton.image = soundOff
		else:
			soundOn=PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\\Images\\unMute.png")), "unMute.png"),master=self.topPresentation)
			self.muteButton.config(image=soundOn)
			self.muteButton.image = soundOn
		self.muteButtonState = muteState


	def setVolumeValue(self,volumeValue) : 
		"""

		Méthode qui change la valeur du VolumeSlider.

		Parametres
		-------------
		volumeValue : float Valeur du volume de la piste.

		"""
		self.volumeSlider.set(volumeValue)


	def setStereoValue(self,stereoValue) :
		"""

		Méthode qui change la valeur du StéréoKnob.

		Parametres
		------------
		stereoValue : float Valeur du stéréo de la piste.

		"""
		self.stereoKnob.setValue(stereoValue)


	def setSpeedValue( self,SpeedValue ) : 
		"""

		Méthode qui change la valeur du SpeedValue.

		Parametres
		------------
		SpeedValue : float Valeur du speed de la piste.
		
		"""
		self.speedKnob.setValue( SpeedValue )

	"""
	--- Méthodes associées aux boutons ---
	"""


	def modifyStereoValue(self):
		"""
		Méthode qui signale au contoller de modifier (si possible) la valeur du stéreo de la piste.
		"""
		self.controller.listenerStereo( self.stereoKnob.getValue() , self.idPiste )

	
	def modifySpeedValue(self):
		"""
		Méthode qui signale au contoller de modifier (si possible) la valeur de la vitesse de la piste.
		"""
		self.controller.listenerPitch( self.speedKnob.getValue() , self.idPiste )

	def modifyVolumeValue(self,vol):
		"""
		Méthode qui signale au contoller de modifier (si possible) la valeur du volume de la piste.
		"""
		self.controller.listenerVolume( int(vol) , self.idPiste )

	def askRemovePiste(self):
		"""
		Méthode qui signale au contoller d'activer ou de désactiver le Random.
		"""
		self.controller.removeChannelID( self.idPiste )

	
	def modifyMuteState(self):
		"""
		Méthode qui signale au contoller d'activer ou désactiver la mise en sourdine.
		"""
		self.controller.listenerMute(not(self.isMuted()), self.getID())