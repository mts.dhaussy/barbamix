#! python3.6
# -*- coding: utf-8 -*-
"""
Groupe Fusée

- Allan RUNEGO
- Clément Quere
- Liam Hô
- Pierre-Alexandre Obin
- Martin Boussion

Scripte python de sauvegarde des paramètres de la table de mixeur dans un fichier en format json
"""
import json

class abstractSave:
    def __init__(self):
        self.dico = {}  #Création du dictionnaire
        
    def getDico(self):
        return self.dico
    
    def setDico(self,dico):
        self.dico = dico
        
    def addNbPiste(self,nbP): #nombre de piste
        self.dico['nb_piste'] = []
        self.dico['nb_piste'].append({'nbp': nbP})
    
    def addSavePiste(self,controllerTop,controllerPiste):    #Enregistre une piste$
        nbP = controllerPiste.getPisteCount()
        self.addNbPiste(nbP)
        
        i = 1
        while(i <= nbP):
            self.dico[i] = []
            #self.dico[i].append(controllerPiste.getTrack(i).export())
            self.dico[i].append({
                'volume': controllerPiste.getVolume(i),
                'stereo': controllerPiste.getStereo(i),
                'pitch': controllerPiste.getPitch(i),
                'mute': controllerPiste.getMute(i),
                'path': controllerPiste.getTrackPath(i),
                'effet': controllerPiste.getEffet(i),
                'crossFade': controllerPiste.getCrossFade(i),
                'randomState': controllerPiste.getRandomState(i),
                'randomPlay': controllerPiste.getRandomPlay(i)
            })
            i+=1
            
        self.addSaveGlobal(controllerTop)

    def addSaveGlobal(self,controllerTop):   #Enregistre le volume global
        self.dico['global'] = []
        self.dico['global'].append({'volume': controllerTop.getVolumeGlobal(),'pause': controllerTop.isPlaying()})
    
    def getValeur(self,piste,valeur):   #Retourne la valeur de la variable
        for i in self.dico[piste]:      #le numéro de la piste
            return i[valeur]            #Retourne la valeur de la variable

    def loadPiste(self,controllerTop,controllerPiste):       #Initialise le track de la piste
        nbPC = controllerPiste.getPisteCount()      #nombre de piste courante
        nbPL = self.getValeur('nb_piste','nbp')     #nombre de piste enregistrer
        
        controllerTop.setVolumeGlobal(self.loadGlobal())
        controllerTop.setPlaying(self.loadPlayingGlobal())
        
        if(nbPC < nbPL):
            controllerPiste.addPistes(nbPL-nbPC)
        else:
            controllerPiste.removePistes(nbPC-nbPL)
        i = 1
        while(i <= nbPL):
            controllerPiste.setVolume(i,self.getValeur(str(i),'volume'))
            controllerPiste.setStereo(i,self.getValeur(str(i),'stereo'))
            controllerPiste.setPitch(i,self.getValeur(str(i),'pitch'))
            controllerPiste.setMute(i,self.getValeur(str(i),'mute'))
            controllerPiste.setTrackPath(i,self.getValeur(str(i),'path'))
            controllerPiste.setEffet(i,self.getValeur(str(i),'effet'))
            controllerPiste.setCrossFade(i,self.getValeur(str(i),'crossFade'))
            controllerPiste.setRandomState(i,self.getValeur(str(i),'randomState'))
            controllerPiste.setRandomPlay(i,self.getValeur(str(i),'randomPlay'))
            i+=1
			
    def loadGlobal(self):
        return self.getValeur('global','volume') #Retourne la valeur du volume global enregistré
		
    def loadPlayingGlobal(self):
        return self.getValeur('global','pause') #Retourne le boolean play/pause